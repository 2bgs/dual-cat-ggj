﻿namespace DualCat.Enums
{
    public enum SFX
    {
        lose,
        win,
        lazer,
        robot,
        jump,
        fall,
        canonShoot,
        canonRotation
    }
}