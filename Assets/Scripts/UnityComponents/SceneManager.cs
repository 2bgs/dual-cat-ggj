﻿using System.Collections;
using DualCat.Utils;
using UnityEngine;

namespace UnityComponents
{
    public class SceneManager: Singleton<SceneManager>
    {
        private bool runOnce = false;
        
        public void LoadScene(string _scene)
        {
            StartCoroutine(_loadScene(_scene));
        }
        
        public void RestartScene()
        {
            if (!runOnce)
            {
                StartCoroutine(_loadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name));
                runOnce = true;
            }
        }
        
        public void RestartSceneImmediately()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
        }
        
        public void LoadSceneImmediately(string name)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(name);
        }

        private IEnumerator _loadScene(string _sceneName)
        {
            yield return new WaitForSeconds(1f);
            UnityEngine.SceneManagement.SceneManager.LoadScene(_sceneName);
        }

        public T[] GetComponentsOnSceneByType<T>() where T : Object
        {
            return FindObjectsOfType<T>();
        }
    }
}