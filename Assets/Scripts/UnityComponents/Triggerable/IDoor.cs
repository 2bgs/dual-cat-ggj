﻿using UnityEngine;

namespace UnityComponents.Triggerable
{
    public interface IDoor
    {
        public void Move(Vector3 velocity);

        public Vector3 GetPosition();
    }
}