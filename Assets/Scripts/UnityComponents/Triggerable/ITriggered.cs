﻿namespace UnityComponents.Triggerable
{
    public interface ITriggered
    {
        public int GetKey();
    }
}