﻿using System;
using UnityEngine;

namespace UnityComponents.Triggerable
{
    public class TriggerCanon: MonoBehaviour, ITrigger
    {
        private bool _isTriggered = false;
        [SerializeField] private int _key = 1;
        
        private void OnTriggerEnter(Collider other)
        {
            _isTriggered = other.CompareTag("Player");
        }

        private void OnTriggerExit(Collider other)
        {
            if (_isTriggered)
            {
                _isTriggered = !other.CompareTag("Player");
            }
        }

        public bool GetIsTriggered()
        {
            if (_isTriggered)
            {
                _isTriggered = false;
                return true;
            }
            return _isTriggered;
        }

        public int GetKey()
        {
            return _key;
        }
    }
}