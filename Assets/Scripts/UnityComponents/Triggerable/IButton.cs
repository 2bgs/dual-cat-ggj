﻿using UnityEngine;

namespace UnityComponents.Triggerable
{
    public interface IButton
    {
        public void Move(Vector3 velocity);

        public Vector3 GetPosition();
    }
}