using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnityComponents.Triggerable
{
    public class Canon : MonoBehaviour, ITriggered, ICanon
    {
        [SerializeField] private GameObject _startPoint;
        [SerializeField] private Vector3 _initDirection;
        [SerializeField] private Vector3 _position;
        [SerializeField] private float _maxRotation = 45.0f;
        [SerializeField] private float _minRotation = -45.0f;
        [SerializeField] private float _rotationSpeed = 10.0f;
        [SerializeField] private GameObject _gun;
        [SerializeField] private int _key;
        [SerializeField] private float _canonSpeed = 50;

        private Collider _gunCollider;
        
        public void Awake()
        {
            _gunCollider = _gun.GetComponent<Collider>();
            _initDirection = _gun.transform.up;
            _position = _gun.transform.position;
        }

        public Vector3 GetCanonStartPoint()
        {
            return _startPoint.transform.position;
        }

        public Vector3 GetCanonDirection()
        {
            return _gun.transform.up;
        }

        public bool RotateGun()
        {
            if (Vector3.Angle(_initDirection, _gun.transform.up) < _maxRotation)
            {
                _gun.transform.RotateAround(_gunCollider.bounds.max, Vector3.back,_rotationSpeed * Time.deltaTime );
                _gun.transform.position = _position;
                    
                return true;
            }

            return false;
        }

        public float GetCanonSpeed()
        {
            return _canonSpeed;
        }
        
        public int GetKey()
        {
            return _key;
        }
    }
}