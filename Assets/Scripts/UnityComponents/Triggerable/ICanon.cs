﻿using UnityEngine;

namespace UnityComponents.Triggerable
{
    public interface ICanon
    {
        public Vector3 GetCanonStartPoint();

        public Vector3 GetCanonDirection();

        public bool RotateGun();
    }
}