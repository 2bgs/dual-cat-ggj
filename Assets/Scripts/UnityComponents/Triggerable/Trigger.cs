﻿using System;
using UnityEngine;

namespace UnityComponents.Triggerable
{
    public class Trigger: MonoBehaviour, ITrigger
    {
        private bool _isTriggered = false;
        [SerializeField] private int _key = 1;
        
        private void OnTriggerEnter(Collider other)
        {
            _isTriggered = true;
        }

        private void OnTriggerExit(Collider other)
        {
            _isTriggered = false;
        }

        public bool GetIsTriggered()
        {
            return _isTriggered;
        }

        public int GetKey()
        {
            return _key;
        }
    }
}