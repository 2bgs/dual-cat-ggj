﻿using UnityEngine;

namespace UnityComponents.Triggerable
{
    public class Door: MonoBehaviour, ITriggered, IDoor
    {
        [SerializeField] private int _key = 1;

        public Vector3 GetPosition()
        {
            return transform.position;
        }

        public void Move(Vector3 velocity)
        {
            transform.Translate(velocity);
        }

        public int GetKey()
        {
            return _key;
        }
    }
}