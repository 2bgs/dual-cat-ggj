﻿using UnityEngine;

namespace UnityComponents.Triggerable
{
    public class Button: MonoBehaviour, IButton
    {
        [SerializeField] private GameObject _button;
        public void Move(Vector3 velocity)
        {
            _button.transform.Translate(velocity);
        }

        public Vector3 GetPosition()
        {
            return _button.transform.position;
        }
    }
}