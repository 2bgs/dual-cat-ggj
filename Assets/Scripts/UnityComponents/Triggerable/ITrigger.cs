﻿namespace UnityComponents.Triggerable
{
    public interface ITrigger
    {
        public bool GetIsTriggered();

        public int GetKey();
    }
}