﻿using DualCat.Utils;
using UnityEditor;
using UnityEngine;

namespace UnityComponents
{
    public class SceneLoader : Singleton<SceneLoader>
    {
        public string NextSceneName;

        public void LoadNextScene()
        {
            SceneManager.Instance.LoadScene(NextSceneName);
        }
    }
}