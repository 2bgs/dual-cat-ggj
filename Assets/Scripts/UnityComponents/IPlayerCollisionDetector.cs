﻿using UnityEngine;

namespace DualCat.Components.Enemy.Controller
{
    public interface IPlayerCollisionDetector
    {
        public bool isCollideWithEnemy();
        public bool isCollideWithGoal();
        public bool isCollideWithWall();
    }
}