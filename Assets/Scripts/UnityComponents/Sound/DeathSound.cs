﻿using DualCat.Utils;
using UnityEngine;

namespace UnityComponents.Sound
{
    [RequireComponent(typeof(AudioSource))]
    public class DeathSound : Singleton<DeathSound>
    {
        private AudioSource _themeSource;

        public float Volume = 0.2f;
        
        void Awake()
        {
            _themeSource = GetComponent<AudioSource>();
            _themeSource.volume = 0;
            
            DeathSound[] objs = FindObjectsOfType<DeathSound>();

            foreach (var o in objs)
            {
                if (o != Instance)
                {
                    Destroy(o.gameObject);
                }
            }

            DontDestroyOnLoad(Instance);
        }
        public void Play(AudioClip audioClip)
        {
            if (_themeSource.clip == null)
            {
                _themeSource.clip = audioClip;
                _themeSource.loop = true;
                _themeSource.Play();
            }
            
        }
        public void EnableVolume()
        {
            _themeSource.volume = Volume;
        }

        public void DisableVolume()
        {
            _themeSource.volume = 0f;
        }
    }
}