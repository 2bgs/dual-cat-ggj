﻿using System.Collections.Generic;
using DualCat.Utils;
using UnityEngine;

namespace UnityComponents.Sound
{
    [RequireComponent(typeof(AudioSource))]
    public class SFXPlayer : Singleton<SFXPlayer>
    {
        private AudioSource _themeSource;
        private List<AudioSource> loopAudios;
        
        public float Volume = 0.2f;
        
        void Awake()
        {
            _themeSource = GetComponent<AudioSource>();
            _themeSource.volume = Volume;
            loopAudios = new List<AudioSource>();
        }

        public void PlayOneShootAudiClip(AudioClip audioClip, float volume = 1.0f)
        {
            _themeSource.PlayOneShot(audioClip, volume);
        }

        public void PlayAudiClipLoop(AudioClip audioClip, float volume = 1.0f)
        {
            var audioSource = gameObject.AddComponent<AudioSource>() as AudioSource;
            audioSource.clip = audioClip;
            audioSource.loop = true;
            audioSource.volume = volume;
            audioSource.Play();
            loopAudios.Add(audioSource);
        }
        
        public void StopAudiClipLoop(AudioClip audioClip)
        { 
            var audioSource = loopAudios.Find(audioSource => audioSource.clip == audioClip);
            if (audioSource)
            {
                audioSource.Stop();
                Destroy(audioSource);
                loopAudios.Remove(audioSource);
            }
        }
    }
}