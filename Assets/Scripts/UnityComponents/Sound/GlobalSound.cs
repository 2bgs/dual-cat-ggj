﻿using DualCat.Utils;
using UnityEngine;

namespace UnityComponents.Sound
{
    [RequireComponent(typeof(AudioSource))]
    public class GlobalSound : Singleton<GlobalSound>
    {
        private AudioSource _themeSource;

        public float Volume = 0.2f;
        
        void Awake()
        {
            _themeSource = GetComponent<AudioSource>();
            _themeSource.volume = Volume;
            
            GlobalSound[] objs = FindObjectsOfType<GlobalSound>();

            foreach (var o in objs)
            {
                if (o != Instance)
                {
                    Destroy(o.gameObject);
                }
            }
            DontDestroyOnLoad(Instance);
        }

        public bool IsPlaying()
        {
            return _themeSource.isPlaying;
        }

        public void PlayAudiClip(AudioClip audioClip)
        {
            _themeSource.PlayOneShot(audioClip);
        }
        
        public void EnableVolume()
        {
            _themeSource.volume = Volume;
        }

        public void DisableVolume()
        {
            _themeSource.volume = 0f;
        }
    }
}