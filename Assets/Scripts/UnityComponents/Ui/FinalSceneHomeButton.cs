﻿using UnityEngine;

namespace UnityComponents.Ui
{
    public class FinalSceneHomeButton : MonoBehaviour
    {
        public void Home()
        {
            SceneManager.Instance.LoadSceneImmediately("Menu");
        }
    }
}