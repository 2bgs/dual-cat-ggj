﻿using System;
using DualCat.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace UnityComponents.Ui
{
    public class BlackOut : Singleton<BlackOut>
    {
        private Image _image;

        private void Awake()
        {
            _image = GetComponent<Image>();
        }

        public void TurnOn()
        {
            _image.enabled = true;
        }

        public void TurnOff()
        {
            _image.enabled = false;
        }
    }
}