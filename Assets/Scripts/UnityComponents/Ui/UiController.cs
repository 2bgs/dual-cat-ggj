﻿using System;
using DualCat.Utils;
using UnityEngine;

namespace UnityComponents.Ui
{
    public class UiController : Singleton<UiController>
    {
        public void InitUi(GameObject buttonUiPrefab)
        {
            Instantiate(buttonUiPrefab, buttonUiPrefab.transform.position, buttonUiPrefab.transform.rotation);
        }

        public void RestartScene()
        {
            SceneManager.Instance.RestartSceneImmediately();
        }

        public void LoadHome()
        {
            SceneManager.Instance.LoadSceneImmediately("Menu");
        }
    }
}