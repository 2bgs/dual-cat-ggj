﻿using UnityEngine;

namespace UnityComponents.Goal
{
    public class GoalMono : MonoBehaviour
    {
        [Header("Animation")]
        public float Amplituted = 0.5f;
        [Range(1, 10f)]
        public double WoblingSpeed = 10f;
        public float RotationSpeed = 10f;
    }
}