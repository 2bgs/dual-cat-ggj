﻿using UnityEditor;

namespace UnityComponents.Editor
{
    [CustomEditor(typeof(SceneLoader))]
    public class GetNextSceneName : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var picker = target as SceneLoader;
            var nextSceneName = picker.NextSceneName;
            
            serializedObject.Update();

            EditorGUI.BeginChangeCheck();
            
            var sceneAsset = EditorGUILayout.ObjectField("Goal Scene Asset", null, typeof(SceneAsset), true) as SceneAsset;
            EditorGUILayout.TextField("Next Scene Name", nextSceneName);

            if (EditorGUI.EndChangeCheck())
            {
                var sceneName = sceneAsset.name;
                var scenePathProperty = serializedObject.FindProperty("NextSceneName");
                scenePathProperty.stringValue = sceneName;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}