﻿using System.Collections.Generic;
using DualCat.Save;
using DualCat.Utils;
using UnityComponents.Ui;

namespace UnityComponents
{
    public class MenuController : Singleton<MenuController>
    {
        private Save _save;

        private void Awake()
        {
            SaveService.LoadGame(out var save);
            _save = save;
            var sceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
            if (save.PassedScenes == null)
            {
                BlackOut.Instance.TurnOn();
                save.PassedScenes = new List<string>();
            }
            if (save.PassedScenes.Count == 0 && sceneName == "Menu")
            {
                SceneManager.Instance.LoadScene("1_Control 1");
            }
            else if (sceneName != "Menu" && !save.PassedScenes.Exists(passedScene => passedScene == sceneName))
            {
                save.PassedScenes.Add(sceneName);
                save.SaveTime = UnixTime.GetCurrentUnixTimeStampInSeconds();
                SaveService.SaveGame(save);
            }
        }

        public bool IsScenePassed(string name)
        {
            return _save.PassedScenes.Exists(i => i == name);
        }
    }
}