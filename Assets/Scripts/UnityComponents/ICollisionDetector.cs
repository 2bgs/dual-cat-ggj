﻿namespace DualCat.Components.Player.Controller
{
    public interface ICollisionDetector
    {
        public bool IsOnGround();
        public bool IsOnCeiling();
    }
}