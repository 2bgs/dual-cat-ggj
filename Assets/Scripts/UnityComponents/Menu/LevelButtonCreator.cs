using UnityEngine;

namespace DualCat.Components.Menu
{
    public class LevelButtonCreator : MonoBehaviour
    {
        [SerializeField] private GameObject _buttonPrefab;

        public GameObject CreateButton()
        {
            var button = Instantiate(_buttonPrefab);
            return button;
        }
    }
}
