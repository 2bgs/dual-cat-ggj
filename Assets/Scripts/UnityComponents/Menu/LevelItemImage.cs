using System;
using System.Linq;
using DualCat.So;
using UnityComponents;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class LevelItemImage : MonoBehaviour
{
    [SerializeField]
    private ImageUiMappingConfig _imageUiMappingConfig;
    
    [SerializeField]
    private Image _frame;
    
    [SerializeField]
    private Image _spriteImage;

    [SerializeField] private string _key;
    
    [SerializeField] private string _sceneName;
    [SerializeField] private Button _button;


    private void Awake()
    {
        _button = GetComponent<Button>();
    }

    private void Start()
    {
        if (MenuController.Instance.IsScenePassed(_sceneName)) {
            _spriteImage.overrideSprite = _imageUiMappingConfig._UiImagesMaps.First(i => i.key == _key).Sprite;
        }
        else
        {
            _spriteImage.overrideSprite = _imageUiMappingConfig._UiImagesMaps.First(i => i.key == "lock").Sprite;
            _button.enabled = false;
        }
        
        var frame = Random.Range(1, 4);
        _frame.overrideSprite = _imageUiMappingConfig._UiImagesMaps.First(i => i.key == "frame" + frame).Sprite;
    }

    public void LoadScene()
    {
        UnityComponents.SceneManager.Instance.LoadSceneImmediately(_sceneName);
    }

    public void ExitGame()
    {
        Application.Quit();
        Debug.Log("Exit!");
    }
}
