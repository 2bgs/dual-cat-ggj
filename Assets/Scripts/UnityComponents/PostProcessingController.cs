﻿using DualCat.Utils;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace DualCat.Components.Player.Controller
{
    public class PostProcessingController : Singleton<PostProcessingController>
    {
        private Volume _volume;
        private ColorCurves _colorCurves;
        private Vignette _vignette;

        private void Awake()
        {
            _volume = GetComponent<Volume>();
            _volume.profile.TryGet<ColorCurves>(out var colorCurves);
            _colorCurves = colorCurves;
            _volume.profile.TryGet<Vignette>(out var vignette);
            _vignette = vignette;
        }

        public void SwitchDeadMode()
        {
            _vignette.active = !_vignette.active;
            _colorCurves.active = !_colorCurves.active;
        }
    }
}