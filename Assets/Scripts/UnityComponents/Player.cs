﻿using DualCat.Components.Enemy.Controller;
using UnityEngine;

namespace DualCat.Components.Player.Controller
{
    public class Player : MonoBehaviour, IMovable, ICollisionDetector, IPlayerCollisionDetector
    {
        private CharacterController _controller;

        [SerializeField] private GameObject _aliveModel;
        [SerializeField] private GameObject _deadModel;

        private bool _isCollideWithEnemy;
        private bool _isCollisionWithGoal = false;

        private Collider _collider;

        private void Awake()
        {
            _controller = GetComponent<CharacterController>();
            _collider = GetComponent<Collider>();
        }

        public void Move(Vector3 v)
        {
            _controller.Move(v);
        }

        public void SetPosition(Vector3 position)
        {
            _controller.enabled = false;
            transform.position = position;
            _controller.enabled = true;
        }

        public bool IsOnGround()
        {
            RaycastHit hit;

            float rayDistance = 0.1f;
            var rayPoint1 = _collider.bounds.min + new Vector3(_collider.bounds.size.x, 0, 0);
            var rayPoint2 = _collider.bounds.min + new Vector3(_collider.bounds.size.x, 0, _collider.bounds.size.z);
            var rayPoint3 = _collider.bounds.min + new Vector3(0, 0, _collider.bounds.size.z);
            var rayPoint4 = _collider.bounds.min;
            var rayPoint5 = _collider.bounds.min + new Vector3(_collider.bounds.size.x / 2, 0, _collider.bounds.size.z / 2);
            Vector3[] rayPoints = {rayPoint1, rayPoint2, rayPoint3, rayPoint4, rayPoint5};

            var isOnGround = false;
            foreach (var rayPoint in rayPoints)
            {
                isOnGround = isOnGround ||
                             (Physics.Raycast(rayPoint, Vector3.down, out hit, rayDistance) &&
                              !hit.collider.isTrigger) && hit.collider.CompareTag("wall");
            }

            return isOnGround || _controller.isGrounded;
        }

        public bool IsOnCeiling()
        {
            RaycastHit hit;

            float rayDistance = 0.1f;

            if (Physics.Raycast(_controller.bounds.max, Vector3.up, out hit, rayDistance))
            {
                return true;
            }

            return false;
        }

        public void SwitchDeadMode()
        {
            _aliveModel.SetActive(!_aliveModel.activeSelf);
            _deadModel.SetActive(!_deadModel.activeSelf);
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Enemy"))
            {
                _isCollideWithEnemy = true;
            }

            if (other.CompareTag("Goal"))
            {
                _isCollisionWithGoal = true;
            }

        }

        void OnTriggerStay(Collider other)
        {
            if (other.CompareTag("Enemy"))
            {
                _isCollideWithEnemy = true;
            }

            if (other.CompareTag("Goal"))
            {
                _isCollisionWithGoal = true;
            }
        }


        void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Enemy"))
            {
                _isCollideWithEnemy = false;
            }
        }

        public bool isCollideWithWall()
        {
            RaycastHit hit;

            float rayDistance = 2.5f;

           
            if (Physics.Raycast((_controller.bounds.max + _controller.bounds.min) / 2, transform.forward, out hit, rayDistance)
            ||
            Physics.Raycast((_controller.bounds.max + _controller.bounds.min) / 2, -transform.forward, out hit, rayDistance)
            )
            {

                return hit.collider.CompareTag("wall");
            }

            return false;
        }

        public bool isCollideWithEnemy()
        {
            return _isCollideWithEnemy;
        }

        public bool isCollideWithGoal()
        {
            return _isCollisionWithGoal;
        }
    }
}