﻿using UnityEngine;

namespace DualCat.Components.Enemy.Controller
{
    public interface IEnemy
    {

        public GameObject GetA();
        public GameObject GetB();
        public Vector3 GetPosition();
    }
}