﻿using UnityEngine;

namespace DualCat.Components.Enemy.Controller
{
    public interface IRobot
    {
        public bool RotateModel(float sign);
        public bool GetIsTriggeredByPlayer();
    }
}