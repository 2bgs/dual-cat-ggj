﻿using UnityEngine;

namespace DualCat.Components.Enemy.Controller
{
    public interface ICollidableWithWall
    {
        public GameObject GetGameObject();
        
        public void DestroyObject(GameObject gameObject);

        public bool GetIsCollideWithWall();
    }
}