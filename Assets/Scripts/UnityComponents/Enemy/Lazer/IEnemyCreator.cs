﻿namespace DualCat.Components.Enemy.Controller
{
    public interface IEnemyCreator
    {
        public Enemy CreateCharge();

        public bool GetIsReload();
    }
}