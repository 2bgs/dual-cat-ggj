﻿using System;
using UnityEngine;

namespace DualCat.Components.Enemy.Controller
{
    public class CollidableWithWall: MonoBehaviour, ICollidableWithWall
    {
        private bool _isCollideWithWall = false;

        public GameObject GetGameObject()
        {
            return gameObject;
        }
        
        public void DestroyObject(GameObject game)
        {
            Destroy(gameObject);
        }

        public bool GetIsCollideWithWall()
        {
            return _isCollideWithWall;
        }

        private void OnTriggerEnter(Collider other)
        {
            _isCollideWithWall = other.CompareTag("wall") || other.CompareTag("Canon") || other.CompareTag("Enemy");
        }

        
    }
}