﻿using System.Collections;
using Components.Controller;
using UnityEngine;

namespace DualCat.Components.Enemy.Controller
{
    public class EnemyCreator: MonoBehaviour, IEnemyCreator
    {
        [SerializeField] private GameObject _charge;
        [SerializeField] private GameObject A;
        [SerializeField] private GameObject B;
        [SerializeField] private float _frequency = 1.2f;
        private bool isReload = false;

        public Enemy CreateCharge()
        {
            
            var charge = Instantiate(_charge);
            var chargeEnemy = charge.GetComponent<Enemy>();
            if (A)
            {
                charge.transform.position = A.transform.position;
                chargeEnemy.setA(A);
            }
            if (B)
            {
                charge.transform.position = B.transform.position;
                chargeEnemy.setB(B);
            }
            isReload = true;
            Invoke("Reload", _frequency);
            return chargeEnemy;
        }

        public void Reload()
        {
            isReload = false;
        }

        public bool GetIsReload()
        {
            return isReload;
        }
    }
}