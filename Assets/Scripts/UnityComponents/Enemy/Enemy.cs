﻿using System;
using DualCat.Components.Player.Controller;
using UnityEngine;

namespace DualCat.Components.Enemy.Controller
{
    public class Enemy : MonoBehaviour, IMovable, ICollisionDetector, IEnemy
    {
        [SerializeField] private GameObject A;
        [SerializeField] private GameObject B;
        private Collider _collider;

        private void Awake()
        {
            _collider = GetComponent<Collider>();
        }

        public void Move(Vector3 v)
        {
            transform.Translate(v);
        }
        
        public void SetPosition(Vector3 position)
        {
            transform.position = position;
        }

        public bool IsOnGround()
        {
            RaycastHit hit;

            float rayDistance = 1f;
            var rayPoint1 = _collider.bounds.min + new Vector3(_collider.bounds.size.x, 0, 0);
            var rayPoint2 = _collider.bounds.min + new Vector3(_collider.bounds.size.x, 0, _collider.bounds.size.z);
            var rayPoint5 = _collider.bounds.min + new Vector3(_collider.bounds.size.x / 2, 0, _collider.bounds.size.z / 2);
            var rayPoint3 = _collider.bounds.min + new Vector3(0, 0, _collider.bounds.size.z);
            var rayPoint4 = _collider.bounds.min;
            Vector3[] rayPoints = { rayPoint1, rayPoint2, rayPoint3, rayPoint4, rayPoint5};
            var isOnGround = false;
            foreach (var rayPoint in rayPoints)
            {
                isOnGround = isOnGround || (Physics.Raycast(rayPoint, Vector3.down, out hit, rayDistance) && !hit.collider.isTrigger);
            }
            return isOnGround;
        }

        public bool IsOnCeiling()
        {
            return false;
        }

        public void SwitchDeadMode()
        {
            
        }

        public void setA(GameObject newA)
        {
            A = newA;
        }
        
        public void setB(GameObject newB)
        {
            B = newB;
        }

        public GameObject GetA()
        {
            return A;
        }

        public GameObject GetB()
        {
            return B;
        }

        public bool isNearB()
        {
            if (B == null)
            {
                return false;
            }
            return Math.Abs(B.transform.position.x - transform.position.x) < 1;
        }

        public Vector3 GetPosition()
        {
            return transform.position;
        }

    }
}