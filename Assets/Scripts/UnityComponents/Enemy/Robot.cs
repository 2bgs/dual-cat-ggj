﻿using System;
using UnityEngine;

namespace DualCat.Components.Enemy.Controller
{
    public class Robot: MonoBehaviour, IRobot
    {
        private bool _isTriggeredByPlayer = false;
        [SerializeField] private GameObject _robotModel;
        [SerializeField] private float _rotationSpeed = 45.0f;
        private Vector3 _initDirection;
        private Collider _collider;

        private void Awake()
        {
            _collider = GetComponent<Collider>();
            _initDirection = _robotModel.transform.forward;
        }

        public bool RotateModel(float sign)
        {
            if (sign == 0.0f)
            {
                return false;
            }
            Vector3 rotatePoint = _collider.bounds.min + new Vector3(_collider.bounds.size.x / 2, 0, _collider.bounds.size.z / 2);
            
            if (Mathf.Abs(Vector3.Dot(_initDirection, _robotModel.transform.forward) + 1) > 0.01f)
            {
                var position = _robotModel.transform.position;
                _robotModel.transform.RotateAround(rotatePoint, Vector3.up,sign * _rotationSpeed * Time.deltaTime);
                _robotModel.transform.position = position;
            } 
            else
            {
                _robotModel.transform.forward = Vector3.Normalize(_robotModel.transform.forward);
                _initDirection = _robotModel.transform.forward;
                return false;
            }

            return true;
        }

        public bool GetIsTriggeredByPlayer()
        {
            return _isTriggeredByPlayer;
        }

        private void OnTriggerEnter(Collider other)
        {
            _isTriggeredByPlayer = other.CompareTag("Player");
        }

        private void OnTriggerExit(Collider other)
        {
            if (_isTriggeredByPlayer)
            {
                _isTriggeredByPlayer = !other.CompareTag("Player");
            }
        }
    }
}