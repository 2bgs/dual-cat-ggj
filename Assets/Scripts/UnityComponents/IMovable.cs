﻿using UnityEngine;

namespace DualCat.Components.Player.Controller
{
    public interface IMovable
    {
        public void Move(Vector3 v);

        public void SetPosition(Vector3 position);
    }
}