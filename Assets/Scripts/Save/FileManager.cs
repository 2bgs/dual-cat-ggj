using System;
using System.IO;
using UnityEngine;

namespace DualCat.Save
{
    public static class FileManager
    {
        public static bool WriteToFile(string a_FileName, string a_FileContents)
        {
            var fullPath = Path.Combine(Application.persistentDataPath, a_FileName);

            try
            {
                File.WriteAllText(fullPath, a_FileContents);
                return true;
            }
            catch (Exception e)
            {
                Debug.LogError($"Failed to write to {fullPath} with exception {e}");
                return false;
            }
        }

        public static bool LoadFromFile(string a_FileName, out string result)
        {
            var fullPath = Path.Combine(Application.persistentDataPath, a_FileName);

            try
            {
                result = File.ReadAllText(fullPath);
                return true;
            }
            catch (FileNotFoundException e)
            {
                Debug.LogWarning($"File not found {fullPath} will be initialize {e}");
                result = "";
                return false;
            }
            catch (Exception e)
            {
                Debug.LogError($"Failed to read from {fullPath} with exception {e}");
                result = "";
                return false;
            }
        }

#if UNITY_EDITOR
        public static string ShowFulltPath()
        {
            return Path.Combine(Application.persistentDataPath);
        }
#endif
    }
}