using System.Runtime.InteropServices;
using UnityEngine;

namespace DualCat.Save
{
    public static class SaveService
    {
        private static string _saveName = "data";

#if UNITY_WEBGL && !UNITY_EDITOR
    [DllImport("__Internal")]
    private static extern void writeData(string name, string data);

    [DllImport("__Internal")]
    private static extern void logData(string data);

    [DllImport("__Internal")]
    private static extern string readData(string name);
#endif
        public static void SaveGame(Save save)
        {
            string dataForSave = JsonUtility.ToJson(save);
#if UNITY_WEBGL && !UNITY_EDITOR
        writeData(_saveName, dataForSave);
#else
            FileManager.WriteToFile(_saveName, dataForSave);
#endif
        }

        public static bool LoadGame(out Save save)
        {
            string loadedData;
            bool result = false;

#if UNITY_WEBGL && !UNITY_EDITOR
        loadedData = readData(_saveName);
        if (loadedData.Length > 0)
        {
            result = true;
        }
#else
            result = FileManager.LoadFromFile(_saveName, out loadedData);
#endif

            if (result)
            {
                save = JsonUtility.FromJson<Save>(loadedData);
            }
            else
            {
                save = new Save();
            }

            return result;
        }
    }
}