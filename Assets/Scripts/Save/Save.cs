using System;
using System.Collections.Generic;

namespace DualCat.Save
{
    public struct Save
    {
        public List<string> PassedScenes;
        public int SaveTime;

        public Save(
            List<string> passedScenes
        )
        {
            PassedScenes = passedScenes;
            SaveTime = UnixTime.GetCurrentUnixTimeStampInSeconds();
        }
    }
}