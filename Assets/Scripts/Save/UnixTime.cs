using System;

namespace DualCat.Save
{
    public static class UnixTime
    {
        public static int ConvertDateToUnixTimeInSeconds(DateTime dt)
        {
            return (int) dt.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
        }

        public static int GetCurrentUnixTimeStampInSeconds()
        {
            return ConvertDateToUnixTimeInSeconds(DateTime.Now);
        }
    }
}