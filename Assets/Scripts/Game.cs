﻿using System.Collections.Generic;
using Systems.Goal;
using DualCat.Components.Audio;
using DualCat.Components.Enemy.Controller;
using DualCat.Components.Gameplay;
using DualCat.Systems.Input;
using DualCat.Systems.Player;
using DualCat.Components.Player.Controller;
using DualCat.So;
using DualCat.Systems.Animator;
using DualCat.Systems.Controller;
using DualCat.Systems.Enemy;
using DualCat.Systems.GamePlay;
using DualCat.Systems.Save;
using DualCat.Systems.Sound;
using DualCat.Systems.Triggered;
using DualCat.Systems.Ui;
using Leopotam.Ecs;
using UnityComponents;
using UnityComponents.Sound;
using UnityComponents.Triggerable;
using UnityComponents.Ui;
using UnityEngine;

namespace DualCat
{
    public class Game : MonoBehaviour
    {
        private EcsWorld _world;
        private EcsSystems _systems;
        
        [SerializeField]
        private WorldConfig _worldConfig;

        [SerializeField] 
        private Player _player;
        [SerializeField] 
        private List<Enemy> _enemies;
        [SerializeField] 
        private List<Robot> _robots;
        [SerializeField]
        private List<EnemyCreator> _enemyCreators;
        [SerializeField]
        private List<Trigger> _buttons;
        [SerializeField]
        private List<Door> _doors;
        [SerializeField]
        private List<UnityComponents.Triggerable.Canon> _canons;

        private void Start()
        {
            _world = new EcsWorld();
            _systems = new EcsSystems(_world);
            _systems.Inject(_player)
                .Inject(_enemies)
                .Inject(_enemyCreators)
                .Inject(_worldConfig)
                .Inject(_buttons)
                .Inject(_doors)
                .Inject(_robots)
                .Inject(_canons)
                .Inject(GlobalSound.Instance)
                .Inject(DeathSound.Instance)
                .Inject(PostProcessingController.Instance)
                .Inject(SceneManager.Instance)
                .Inject(SceneLoader.Instance)
                .Inject(SFXPlayer.Instance)
                .Inject(UiController.Instance)
                
                //Init Systems
                .Add(new InitUiSystem())
                .Add(new InitGoalSystem())
                .Add(new SaveInitSystem())
                .Add(new PlayerInitSystem())
                .Add(new EnemyInitSystem())
                .Add(new RobotInitSystem())
                .Add(new EnemyCreatorInitSystem())
                .Add(new ButtonInitSystem())
                .Add(new CanonInitSystem())
                
                //Run Systems
                .Add(new GoalAnimationSystem())
                .Add(new MusicControlSystem())
                .Add(new WinLevelSystem())
                .Add(new LoadNextLevelSystem())
                .Add(new ReadInputSystem())
                .Add(new ProcessEnemySystem())
                .Add(new ProcessRobotSystem())
                .Add(new ProcessEnemyCreationSystem())
                .Add(new GravitySystem())
                .Add(new ProcessInputSystem())
                .Add(new ChangeStateSystem())
                .Add(new ProcessTriggerCanons())
                .Add(new MoveCalculation())
                .Add(new MoveSystem())
                .Add(new ProcessMovingAnimation())
                .Add(new ProcessTriggerButton())
                .Add(new ProcessTriggerDoor())
                .Add(new ProcessEnemyCollideWithWallSystem())
                .Add(new DestroyGoalOnWinSystem())
                .Add(new ProcessRestartSystem())
                .Add(new SFXProcessSystem())
                .OneFrame<HorizontalMove>()
                .OneFrame<VerticalMove>()
                .OneFrame<Win>()
                .OneFrame<Restart>()
                .OneFrame<Jump>()
                .OneFrame<Fall>()
                .OneFrame<LazerShoot>()
                .OneFrame<RobotMoving>()
                .OneFrame<CanonShoot>()
                .Init();
        }
        
        private void Update()
        {
            _systems?.Run();
        }
    
        private void OnDestroy()
        {
            _systems?.Destroy();
            _systems = null;
            _world?.Destroy();
            _world = null;
        }

    }
}