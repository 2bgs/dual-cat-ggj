﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace DualCat.So
{
    [CreateAssetMenu(fileName = "menuUi", menuName = "So/ImageUiMappingConfig", order = 0)]
    public class ImageUiMappingConfig : ScriptableObject
    {
        [Header("Ui")]
        public List<UiImagesMap> _UiImagesMaps;
        
        [Serializable]
        public struct UiImagesMap
        {
            public string key;
            public Sprite Sprite;
        }
    }
}