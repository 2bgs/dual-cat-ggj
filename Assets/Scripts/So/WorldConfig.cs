﻿using System;
using System.Collections.Generic;
using DualCat.Enums;
using UnityEngine;

namespace DualCat.So
{
    [CreateAssetMenu(fileName = "WorldConfig", menuName = "SO/WorldCOnfig", order = 0)]
    public class WorldConfig : ScriptableObject
    {
        public float GravityForce;
        public float PlayerHorizontalSpeed;
        public float PlayerVerticalSpeed;
        public float EnemyHorizontalSpeed;
        public float LazerChargeHorizontalSpeed;

        [Header("Ui")]
        public GameObject _UiPrefab;

        [Header("Music")]
        public AudioClip[] Themes;

        [Header("Death Theme")]
        public AudioClip DeathTheme;
        
        [Header("SFX")]
        public List<SfxClip> SFX;
        
        [Serializable]
        public struct SfxClip
        {
            public SFX Type;
            public AudioClip AudioClip;
        }
    }
}