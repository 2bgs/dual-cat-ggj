﻿using DualCat.Systems.GamePlay;
using DualCat.Systems.Menu;
using DualCat.Systems.Save;
using Leopotam.Ecs;
using UnityEngine;

namespace DualCat
{
    public class Menu : MonoBehaviour
    {
        private EcsWorld _world;
        private EcsSystems _systems;
        [SerializeField] private DualCat.Components.Menu.LevelButtonCreator _levelButtonCreator;
        private void Start()
        {
            _world = new EcsWorld();
            _systems = new EcsSystems(_world);
            _systems.Inject(_levelButtonCreator)
                .Add(new SaveInitSystem())
                .Add(new MenuInitSystem())
                .Init();
        }
        
        private void Update()
        {
            _systems?.Run();
        }
    
        private void OnDestroy()
        {
            _systems?.Destroy();
            _systems = null;
            _world?.Destroy();
            _world = null;
        }
    }
}