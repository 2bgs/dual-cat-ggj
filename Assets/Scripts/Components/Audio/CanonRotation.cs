﻿using Leopotam.Ecs;

namespace DualCat.Components.Audio
{
    public struct CanonRotation
    {
        public bool IsBeginRotate;
        public bool IsRotating;
        public bool IsStopRotating;
    }
}