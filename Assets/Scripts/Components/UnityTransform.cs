﻿using UnityEngine;

namespace DualCat.Components
{
    public struct UnityTransform
    {
        public Transform Transform;
    }
}