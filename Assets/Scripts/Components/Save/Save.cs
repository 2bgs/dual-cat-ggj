﻿using System.Collections.Generic;

namespace DualCat.Components.Save
{
    public struct Save
    {
        public List<string> PassedScenesNames;
    }
}