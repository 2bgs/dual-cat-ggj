﻿using UnityEngine;

namespace DualCat.Components
{
    public struct UnityGameObject
    {
        public GameObject GameObject;
    }
}