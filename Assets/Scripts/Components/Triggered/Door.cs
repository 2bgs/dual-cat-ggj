﻿using UnityComponents.Triggerable;
using UnityEngine;

namespace DualCat.Components.Triggered
{
    public struct Door
    {
        public IDoor door;
        public Vector3 doorInitPosition;
    }
}