﻿using UnityComponents.Triggerable;
using UnityEngine;

namespace DualCat.Components.Triggered
{
    public struct Button
    {
        public IButton button;
        public Vector3 buttonInitPosition;
    }
}