﻿using UnityComponents.Triggerable;

namespace DualCat.Components.Triggered
{
    public struct Triggered
    {
        public ITrigger Trigger;
        public ITriggered triggered;
    }
}