﻿using UnityComponents.Triggerable;

namespace DualCat.Components.Triggered
{
    public struct Canon
    {
        public ICanon ICanon;
    }
}