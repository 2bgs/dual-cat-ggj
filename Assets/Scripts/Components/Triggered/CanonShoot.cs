﻿namespace DualCat.Components.Triggered
{
    public struct CanonShoot
    {
        public bool IsEntityFlying;
        public bool IsEntityShooted;
        public float CanonSpeed;
    }
}