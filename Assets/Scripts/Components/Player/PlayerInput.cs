﻿namespace Components.Player
{
    public struct PlayerInput
    {
        public float Horizontal;
        public float Vertical;
        public bool SwitchDeadState;
    }
}