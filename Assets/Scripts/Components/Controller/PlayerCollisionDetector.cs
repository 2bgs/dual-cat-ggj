﻿using DualCat.Components.Enemy.Controller;

namespace Components.Controller
{
    public struct PlayerCollisionDetector
    {
        public IPlayerCollisionDetector Detector;
    }
}