﻿using DualCat.Components.Player.Controller;
using UnityEngine;

namespace Components.Controller
{
    public struct Movable
    {
        public IMovable Controller;
        public Vector3 finalMoveVector;
        public float speed;
        public float jumpSpeed;
    }
}