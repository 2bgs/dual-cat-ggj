﻿using DualCat.Components.Enemy.Controller;

namespace Components.Controller
{
    public struct CollidableWithWallComponent
    {
        public ICollidableWithWall CollidableWithWall;
    }
}