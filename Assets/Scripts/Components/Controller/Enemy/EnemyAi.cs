﻿using DualCat.Components.Enemy.Controller;
using UnityEngine;

namespace Components.Controller
{
    public struct EnemyAi
    {
        public IEnemy AiTools;
        public GameObject A;
        public GameObject B;
    }
}