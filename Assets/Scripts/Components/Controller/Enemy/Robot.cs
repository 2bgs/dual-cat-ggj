﻿using DualCat.Components.Enemy.Controller;

namespace Components.Controller
{
    public struct Robot
    {
        public IRobot IRobot;
        public float Speed;
        public bool Rotate;
        public float RotateSign;
    }
}