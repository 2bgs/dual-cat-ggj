﻿using DualCat.Components.Enemy.Controller;

namespace Components.Controller
{
    public struct EnemyCreator
    {
        public IEnemyCreator Creator;
    }
}