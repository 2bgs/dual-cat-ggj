﻿using DualCat.Components.GameControl.Controller;
using DualCat.So;
using Leopotam.Ecs;
using UnityComponents.Sound;
using UnityEngine;

namespace DualCat.Systems.Sound
{
    public class MusicControlSystem: IEcsRunSystem
    {
        private EcsFilter<Death> _death;
        private GlobalSound _globalSound;
        private DeathSound _deathSound;
        private WorldConfig _worldConfig;
        
        public void Run()
        {
            if (!_death.IsEmpty())
            {
                _globalSound.DisableVolume();
                _deathSound.Play(_worldConfig.DeathTheme);
                _deathSound.EnableVolume();
            }
            else
            {
                _globalSound.EnableVolume();
                _deathSound.DisableVolume();
                
            }

            if (_globalSound.IsPlaying() || _worldConfig.Themes.Length == 0) return;

            _globalSound.PlayAudiClip(_worldConfig.Themes[Random.Range(0, _worldConfig.Themes.Length)]);
        }
    }
}