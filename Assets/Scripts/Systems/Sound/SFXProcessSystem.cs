﻿using System.Linq;
using DualCat.Components.Audio;
using DualCat.Components.Gameplay;
using DualCat.Enums;
using DualCat.So;
using Leopotam.Ecs;
using UnityComponents.Sound;
using UnityEngine;

namespace DualCat.Systems.Sound
{
    public class SFXProcessSystem: IEcsRunSystem
    {
        private EcsFilter<Restart> _restart;
        private EcsFilter<Win> _win;
        private EcsFilter<LazerShoot> _lazer;
        private EcsFilter<RobotMoving> _robot;
        private EcsFilter<Jump> _jump;
        private EcsFilter<Fall> _fall;
        private EcsFilter<CanonShoot> _canonShoot;
        private EcsFilter<CanonRotation> _canonRotation;

        private WorldConfig _worldConfig;
        private SFXPlayer _sfxPlayer;
        
        public void Run()
        {
            if (!_restart.IsEmpty())
            {
                var _clip = _worldConfig.SFX.First(clip => clip.Type == SFX.lose).AudioClip;
                _sfxPlayer.PlayOneShootAudiClip(_clip);
            }
            
            if (!_win.IsEmpty())
            {
                var _clip = _worldConfig.SFX.First(clip => clip.Type == SFX.win).AudioClip;
                _sfxPlayer.PlayOneShootAudiClip(_clip);
            }

            foreach (var i in _lazer)
            {
                var _clip = _worldConfig.SFX.Find(clip => clip.Type == SFX.lazer);
                _sfxPlayer.PlayOneShootAudiClip(_clip.AudioClip, 0.5f);
            }

            foreach (var i in _robot)
            {
                var _clip = _worldConfig.SFX.Find(clip => clip.Type == SFX.robot);
                _sfxPlayer.PlayAudiClipLoop(_clip.AudioClip, 0.15f);
            }

            foreach (var i in _jump)
            {
                var _clip = _worldConfig.SFX.Find(clip => clip.Type == SFX.jump);
                _sfxPlayer.PlayOneShootAudiClip(_clip.AudioClip);
            }

            foreach (var i in _fall)
            {
                var _clip = _worldConfig.SFX.Find(clip => clip.Type == SFX.fall);
                _sfxPlayer.PlayOneShootAudiClip(_clip.AudioClip);
            }

            foreach (var i in _canonShoot)
            {
                var _clip = _worldConfig.SFX.Find(clip => clip.Type == SFX.canonShoot);
                _sfxPlayer.PlayOneShootAudiClip(_clip.AudioClip);
            }

            foreach (var i in _canonRotation)
            {
                var _clip = _worldConfig.SFX.Find(clip => clip.Type == SFX.canonRotation);
                if (_canonRotation.Get1(i).IsBeginRotate && !_canonRotation.Get1(i).IsRotating)
                {
                    _sfxPlayer.PlayAudiClipLoop(_clip.AudioClip);
                    _canonRotation.Get1(i).IsRotating = true;
                }
                else if (_canonRotation.Get1(i).IsStopRotating && _canonRotation.Get1(i).IsRotating)
                {
                    _sfxPlayer.StopAudiClipLoop(_clip.AudioClip);
                    _canonRotation.Get1(i).IsRotating = false;
                    _canonRotation.Get1(i).IsStopRotating = false;
                }

                _canonRotation.Get1(i).IsBeginRotate = false;
            }

        }
    }
}