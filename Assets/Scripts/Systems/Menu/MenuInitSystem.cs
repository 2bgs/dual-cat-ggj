﻿using DualCat.Components.Save;
using Leopotam.Ecs;
using UnityComponents;
using UnityEngine;
using UnityEngine.UI;

namespace DualCat.Systems.Menu
{
    public class MenuInitSystem: IEcsInitSystem
    {
        private DualCat.Components.Menu.LevelButtonCreator _levelButtonCreator;
        private EcsFilter<Components.Save.Save> _save;

        public void Init()
        {
            var position = Vector2.zero;
            foreach (var passedScenesName in _save.Get1(0).PassedScenesNames)
            {
                var button = _levelButtonCreator.CreateButton();
                var sceneLoaderComponent = button.GetComponent<SceneLoader>();
                sceneLoaderComponent.NextSceneName = passedScenesName;
                var textComponent = button.GetComponentInChildren<Text>();
                textComponent.text = passedScenesName;
                button.transform.parent = _levelButtonCreator.transform;
                var rectTransform = button.GetComponent<RectTransform>();
                rectTransform.anchoredPosition = position;
                position.y -= 80;
            }
        }
    }
}