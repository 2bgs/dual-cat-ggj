﻿using Components.Controller;
using Components.Player;
using DualCat.Components.Player.Controller;
using DualCat.So;
using Leopotam.Ecs;

namespace DualCat.Systems.Player
{
    public class PlayerInitSystem: IEcsInitSystem
    {
        private Components.Player.Controller.Player _player;
        private EcsWorld _world;
        private WorldConfig _worldConfig;
        private PlayerCollisionDetector _playerCollisionDetector;
        
        public void Init()
        {
            var entity = _world.NewEntity();
            entity.Get<PlayerInput>();
            ref var groundSensor = ref entity.Get<CollisionSensor>();
            ref var movableComponent = ref entity.Get<Movable>();
            ref var playerCollisionDetector = ref entity.Get<PlayerCollisionDetector>();
            ref var animatorComponent = ref entity.Get<Components.Animator>();
            movableComponent.Controller = _player;
            movableComponent.speed = _worldConfig.PlayerHorizontalSpeed;
            movableComponent.jumpSpeed = _worldConfig.PlayerVerticalSpeed;
            groundSensor.Sensor = _player;
            playerCollisionDetector.Detector = _player;
            animatorComponent.component = _player.GetComponentInChildren<UnityEngine.Animator>();
        }
    }
}