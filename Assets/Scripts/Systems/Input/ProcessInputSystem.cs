﻿using Components.Controller;
using Components.Player;
using DualCat.Components.Audio;
using DualCat.Components.GameControl.Controller;
using DualCat.Components.Player;
using DualCat.Components.Player.Controller;
using Leopotam.Ecs;
using UnityEngine;

namespace DualCat.Systems.Input
{
    public class ProcessInputSystem: IEcsRunSystem
    {
        private EcsFilter<PlayerInput, Movable, CollisionSensor> _player;
        
        private EcsFilter<TakeControl> _takeContorl;
        
        public void Run()
        {
            var isTakeControl = !_takeContorl.IsEmpty();
            if (isTakeControl)
            {
                if (_player.Get3(0).Sensor.IsOnGround() || _player.Get3(0).Sensor.IsOnCeiling())
                {
                    _player.Get2(0).finalMoveVector.x = 0;
                }
                return;
            }
            var horizontalMovement = isTakeControl ? 0 : _player.Get1(0).Horizontal;
            _player.Get2(0).finalMoveVector.x = horizontalMovement * _player.Get2(0).speed;

            var verticalMovement = isTakeControl ? 0 : _player.Get1(0).Vertical;
            
            if (verticalMovement > 0 && _player.Get3(0).Sensor.IsOnGround())
            {
                _player.Get2(0).finalMoveVector.y = _player.Get2(0).jumpSpeed;
                _player.GetEntity(0).Get<Jump>();
            }
        }
    }
}