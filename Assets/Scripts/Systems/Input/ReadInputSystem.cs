﻿using Components.Player;
using DualCat.Components.Player;
using Leopotam.Ecs;
using UnityEngine;

namespace DualCat.Systems.Input
{
    public class ReadInputSystem: IEcsRunSystem
    {
        private EcsFilter<PlayerInput> _player;
        
        public void Run()
        {
            _player.Get1(0).Horizontal = UnityEngine.Input.GetAxis("Horizontal");
            _player.Get1(0).Vertical = UnityEngine.Input.GetAxis("Vertical");
            if (_player.Get1(0).Vertical == 0)
            {
                _player.Get1(0).Vertical = UnityEngine.Input.GetKeyDown(KeyCode.Space) ? 1 : 0;
            }
                
            _player.Get1(0).SwitchDeadState = UnityEngine.Input.GetKeyDown(KeyCode.F);
        }
    }
}