﻿using System.Collections.Generic;
using System.Linq;
using DualCat.Save;
using Leopotam.Ecs;
using UnityComponents;
using UnityEngine;

namespace DualCat.Systems.Save
{
    public class SaveInitSystem: IEcsInitSystem
    {
        
        private EcsWorld _world;

        public void Init()
        {
            var entity = _world.NewEntity();
            ref Components.Save.Save entitySave = ref entity.Get<Components.Save.Save>();
            SaveService.LoadGame(out var save);
            var sceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
            if (save.PassedScenes == null)
            {
                save.PassedScenes = new List<string>();
            }
            if (save.PassedScenes.Count == 0 && sceneName == "Menu")
            {
                SceneManager.Instance.LoadScene("1_Control");
            }
            else if (sceneName != "Menu" && !save.PassedScenes.Exists(passedScene => passedScene == sceneName))
            {
                save.PassedScenes.Add(sceneName);
                save.SaveTime = UnixTime.GetCurrentUnixTimeStampInSeconds();
                SaveService.SaveGame(save);
            }
            entitySave.PassedScenesNames = save.PassedScenes;
        }
    }
}