﻿using DualCat.Components.Gameplay;
using Leopotam.Ecs;
using UnityComponents;

namespace DualCat.Systems.GamePlay
{
    public class LoadNextLevelSystem: IEcsRunSystem
    {
        private EcsFilter<Win> _winCondition;
        private SceneLoader _sceneLoader;
        
        public void Run()
        {
            foreach (var i in _winCondition)
            {
                _sceneLoader.LoadNextScene();
            }
        }
    }
}