﻿using System.Linq;
using Components.Controller;
using DualCat.Components.Gameplay;
using DualCat.Enums;
using DualCat.So;
using Leopotam.Ecs;
using UnityComponents.Sound;

namespace DualCat.Systems.GamePlay
{
    public class WinLevelSystem: IEcsRunSystem
    {
        private EcsFilter<PlayerCollisionDetector> _playerCollision;
        private WorldConfig _worldConfig;
        private SFXPlayer _sfxPlayer;
        private EcsWorld _ecsWorld;

        public void Run()
        {
            foreach (var i in _playerCollision)
            {
                if (_playerCollision.Get1(i).Detector.isCollideWithGoal())
                {
                    _playerCollision.GetEntity(i);
                    _ecsWorld.NewEntity().Get<Win>();
                    _playerCollision.GetEntity(i).Del<PlayerCollisionDetector>();
                }
            }
        }
    }
}