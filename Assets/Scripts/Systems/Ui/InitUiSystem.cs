﻿using DualCat.So;
using Leopotam.Ecs;
using UnityComponents.Ui;

namespace DualCat.Systems.Ui
{
    public class InitUiSystem: IEcsInitSystem
    {
        private WorldConfig _worldConfig;
        private UiController _uiController;
        
        public void Init()
        {
            _uiController.InitUi(_worldConfig._UiPrefab);
        }
    }
}