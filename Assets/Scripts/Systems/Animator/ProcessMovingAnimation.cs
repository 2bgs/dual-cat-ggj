﻿using Components.Controller;
using Components.Player;
using Leopotam.Ecs;
using UnityEngine;

namespace DualCat.Systems.Animator
{
    public class ProcessMovingAnimation: IEcsRunSystem
    {
        private EcsFilter<Components.Animator, Movable> _movingAnimation;
        public void Run()
        {
            foreach (var i in _movingAnimation)
            {
                ref var movableComponent = ref _movingAnimation.Get2(i);
                ref var animatorComponent = ref _movingAnimation.Get1(i);
                animatorComponent.component.SetFloat("horizontal", movableComponent.finalMoveVector.x);
                animatorComponent.component.SetFloat("vertical", movableComponent.finalMoveVector.y);
            }
        }
    }
}