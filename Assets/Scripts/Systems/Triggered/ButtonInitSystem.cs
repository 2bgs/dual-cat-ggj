﻿using System.Collections.Generic;
using Leopotam.Ecs;
using UnityComponents.Triggerable;
using Components.Controller;
using Door = UnityComponents.Triggerable.Door;


namespace DualCat.Systems.Triggered
{
    public class ButtonInitSystem: IEcsInitSystem
    {
        private List<Trigger> _buttons;
        private List<Door> _doors;
        private EcsWorld _world;

        public void Init()
        {
            foreach (var _button in _buttons)
            {
                foreach (var _door in _doors)
                {
                    if (_door.GetKey() == _button.GetKey())
                    {
                        var entity = _world.NewEntity();
                        ref var triggered = ref entity.Get<Components.Triggered.Triggered>();
                        triggered.triggered = _door;
                        triggered.Trigger = _button;
                        ref var button = ref entity.Get<Components.Triggered.Button>();
                        button.button = _button.gameObject.GetComponent<Button>();
                        button.buttonInitPosition = button.button.GetPosition();
                        ref var door = ref entity.Get<Components.Triggered.Door>();
                        door.door = _door;
                        door.doorInitPosition = _door.transform.position;
                    }
                }
            }
        }
    }
}