﻿using DualCat.Components.Triggered;
using Leopotam.Ecs;
using UnityEngine;

namespace DualCat.Systems.Triggered
{
    public class ProcessTriggerButton: IEcsRunSystem
    {
        private EcsFilter<Components.Triggered.Triggered, Button> _triggered;
        
        public void Run()
        {
            foreach (var i in _triggered)
            {
                if (_triggered.Get1(i).Trigger.GetIsTriggered())
                {
                    if (Vector3.Magnitude(_triggered.Get2(i).button.GetPosition() -
                                          _triggered.Get2(i).buttonInitPosition) <
                        0.5f)
                    {
                        _triggered.Get2(i).button.Move(new Vector3(0f, -0.1f, 0));
                    }
                }
                else 
                {
                    if (_triggered.Get2(i).button.GetPosition() != _triggered.Get2(i).buttonInitPosition)
                    {
                        _triggered.Get2(i).button.Move(new Vector3(0f, 0.1f, 0));
                    }
                }
            }
        }
    }
}