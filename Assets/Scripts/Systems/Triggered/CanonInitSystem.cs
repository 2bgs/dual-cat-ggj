﻿using System.Collections.Generic;
using Leopotam.Ecs;
using UnityComponents.Triggerable;
using Components.Controller;
using DualCat.Components.Triggered;
using DualCat.So;
using UnityEngine;
using Button = UnityComponents.Triggerable.Button;
using Canon = UnityComponents.Triggerable.Canon;
using Door = UnityComponents.Triggerable.Door;


namespace DualCat.Systems.Triggered
{
    public class CanonInitSystem: IEcsInitSystem
    {
        private List<Trigger> _buttons;
        private List<Canon> _canons;
        private EcsWorld _world;
        private WorldConfig _worldConfig;

        public void Init()
        {
            foreach (var _canon in _canons)
            {
                var canonEntity = _world.NewEntity();
                initButton(_canon.gameObject, _canon, canonEntity);
                ref var canonShoot = ref canonEntity.Get<CanonShoot>();
                canonShoot.IsEntityFlying = false;
                canonShoot.CanonSpeed = _canon.GetCanonSpeed();
                foreach (var _button in _buttons)
                {
                    var buttonCanonEntity = _world.NewEntity();
                    initButton(_button.gameObject, _canon, buttonCanonEntity);
                }
            }
            
            
        }

        private void initButton(GameObject _button, Canon _canon, EcsEntity buttonCanonEntity)
        {
            ref var canonButton = ref buttonCanonEntity.Get<Components.Triggered.Canon>();
            canonButton.ICanon = _canon;

            if (_canon.GetKey() == _button.GetComponent<ITrigger>().GetKey())
            {
                ref var triggered = ref buttonCanonEntity.Get<Components.Triggered.Triggered>();
                triggered.triggered = _canon;
                triggered.Trigger = _button.GetComponent<ITrigger>();

                var buttonComopnent = _button.GetComponent<Button>();
                if (buttonComopnent)
                {
                    ref var button = ref buttonCanonEntity.Get<Components.Triggered.Button>();
                    button.button = buttonComopnent;
                    button.buttonInitPosition = button.button.GetPosition();
                }
            }
        }
    }
}