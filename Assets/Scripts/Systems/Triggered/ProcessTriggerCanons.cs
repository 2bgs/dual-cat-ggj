﻿using Components.Controller;
using Components.Player;
using DualCat.Components.Audio;
using DualCat.Components.GameControl.Controller;
using DualCat.Components.Gameplay;
using DualCat.Components.Player.Controller;
using DualCat.Components.Triggered;
using Leopotam.Ecs;
using UnityEngine;
using CanonShoot = DualCat.Components.Audio.CanonShoot;

namespace DualCat.Systems.Triggered
{
    public class ProcessTriggerCanons: IEcsRunSystem
    {
        private EcsFilter<Components.Triggered.Triggered, Canon, Button> _canonButton;
        private EcsFilter<Components.Triggered.Triggered, Canon, DualCat.Components.Triggered.CanonShoot> _canonShoot;
        private EcsFilter<Movable, CollisionSensor, PlayerCollisionDetector> _player;
        private EcsWorld _ecsWorld;
        private EcsFilter<Death> _death;
        private EcsFilter<TakeControl> _takeControl;
        private EcsFilter<Fall> _fall;
        private EcsFilter<DualCat.Components.Audio.CanonShoot> _canonShootAudio;
        private EcsFilter<CanonRotation> _canonRotation;
        private EcsFilter<Restart> _restart;
        
        public void Run()
        {
            foreach (var i in _canonButton)
            { 
                if (_canonButton.Get1(i).Trigger.GetIsTriggered())
                {
                    if (_canonButton.Get2(i).ICanon.RotateGun())
                    {
                        _canonButton.GetEntity(i).Get<DualCat.Components.Audio.CanonRotation>().IsBeginRotate = true;    
                    }
                    else
                    {
                        ref var canonRotation = ref _canonButton.GetEntity(i).Get<DualCat.Components.Audio.CanonRotation>();
                        canonRotation.IsStopRotating = true;
                    }
                }
                else
                {
                    ref var canonRotation = ref _canonButton.GetEntity(i).Get<DualCat.Components.Audio.CanonRotation>();
                    canonRotation.IsStopRotating = true;
                }
            }
            foreach (var i in _canonShoot)
            {
                
                if (_canonShoot.Get1(i).Trigger.GetIsTriggered() && 
                    !_canonShoot.Get3(i).IsEntityFlying &&
                   _restart.IsEmpty()
                    ) {
                    _player.Get1(0).Controller.SetPosition(_canonShoot.Get2(i).ICanon.GetCanonStartPoint());
                    _canonShoot.Get3(i).IsEntityFlying = true;
                    _canonShoot.Get3(i).IsEntityShooted = true;
                    _ecsWorld.NewEntity().Get<TakeControl>();
                    _ecsWorld.NewEntity().Get<DualCat.Components.Audio.CanonShoot>();
                }

                if (_canonShoot.Get3(i).IsEntityFlying &&
                    (_player.Get2(0).Sensor.IsOnGround() || 
                     _player.Get2(0).Sensor.IsOnCeiling() ||
                     _player.Get3(0).Detector.isCollideWithWall()))
                {
                    
                    if (_death.IsEmpty())
                    {
                        _ecsWorld.NewEntity().Get<Restart>();
                    }

                    _player.Get1(0).finalMoveVector = new Vector3(0, 0, 0);
                    _canonShoot.Get3(i).IsEntityFlying = false;
                    if (!_takeControl.IsEmpty())
                    {
                        foreach(var j in _takeControl)
                        {
                            _takeControl.GetEntity(j).Del<TakeControl>();
                            break;
                        }
                    }
                }
                if (_canonShoot.Get3(i).IsEntityShooted)
                {
                    var direction = _canonShoot.Get2(i).ICanon.GetCanonDirection();
                    direction.z = 0;
                    _player.Get1(0).finalMoveVector = _canonShoot.Get3(i).CanonSpeed * direction.normalized;
                    _canonShoot.Get3(i).IsEntityShooted = false;
                }

            }

        }
    }
}