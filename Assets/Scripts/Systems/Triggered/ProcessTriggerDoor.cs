﻿using DualCat.Components.Triggered;
using Leopotam.Ecs;
using UnityEngine;

namespace DualCat.Systems.Triggered
{
    public class ProcessTriggerDoor: IEcsRunSystem
    {
        private EcsFilter<Components.Triggered.Triggered, Door, Button> _triggered;
        
        public void Run()
        {
            foreach (var i in _triggered)
            {
                if (_triggered.Get1(i).Trigger.GetIsTriggered())
                {
                    if (Vector3.Magnitude(_triggered.Get2(i).door.GetPosition() - _triggered.Get2(i).doorInitPosition) <
                        8.0f)
                    {
                        _triggered.Get2(i).door.Move(new Vector3(0, 0, 0.5f));   
                    }
                }
                else  {
                    if (_triggered.Get2(i).door.GetPosition() != _triggered.Get2(i).doorInitPosition)
                    {
                        _triggered.Get2(i).door.Move(new Vector3(0, 0, -0.5f));
                    }
                }
            }
        }
    }
}