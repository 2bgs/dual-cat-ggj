﻿using Components.Controller;
using Leopotam.Ecs;
using UnityEngine;

namespace DualCat.Systems.Controller
{
    public class MoveSystem: IEcsRunSystem
    {
        private EcsFilter<Movable> _movable;
        
        public void Run()
        {
            foreach (var i in _movable)
            {
                ref var movableComponent = ref _movable.Get1(i);
                movableComponent.Controller.Move(movableComponent.finalMoveVector * Time.deltaTime);
            }
        }
    }
}