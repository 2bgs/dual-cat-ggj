﻿using Components.Player;
using DualCat.Components.GameControl.Controller;
using DualCat.Components.Player.Controller;
using Leopotam.Ecs;

namespace DualCat.Systems.Controller
{
    public class ChangeStateSystem: IEcsRunSystem
    {
        private EcsFilter<PlayerInput> _playerInput;
        private PostProcessingController _processingController;
        private Components.Player.Controller.Player _player;
        private EcsFilter<Death> _death;
        private EcsFilter<TakeControl> _takeControl;

        private EcsWorld _ecsWorld;
        
        public void Run()
        {
            if (_playerInput.Get1(0).SwitchDeadState)
            {
                if (_death.IsEmpty())
                {
                    _ecsWorld.NewEntity().Get<Death>();
                    _ecsWorld.NewEntity().Get<TakeControl>();
                }
                else
                {
                    _death.GetEntity(0).Del<Death>();
                    if (!_takeControl.IsEmpty())
                    {
                        foreach(var i in _takeControl)
                        {
                            _takeControl.GetEntity(i).Del<TakeControl>();
                            break;
                        }
                    }
                }

                _processingController.SwitchDeadMode();
                _player.SwitchDeadMode();
            }
        }
    }
}