﻿using Components.Controller;
using DualCat.Components.Audio;
using DualCat.Components.Player.Controller;
using DualCat.So;
using Leopotam.Ecs;
using UnityEngine;

namespace DualCat.Systems.Controller
{
    public class GravitySystem: IEcsRunSystem
    {
        private EcsFilter<CollisionSensor, Movable> _groundSensor;
        private EcsFilter<Fall> _fall;
        private WorldConfig _worldConfig;
        private EcsWorld _ecsWorld;

        public void Run()
        {
            foreach (var i in _groundSensor)
            {
                if (!_groundSensor.Get1(i).Sensor.IsOnGround())
                {
                    _groundSensor.GetEntity(i).Get<VerticalMove>().v = -_worldConfig.GravityForce * Time.deltaTime;
                }
               
                if (_groundSensor.Get1(i).Sensor.IsOnCeiling() && _groundSensor.GetEntity(i).Get<Movable>().finalMoveVector.y >= 0)
                {
                    _groundSensor.GetEntity(i).Get<Movable>().finalMoveVector.y = 0;
                }

                if (_groundSensor.Get1(i).Sensor.IsOnGround())
                {
                    _groundSensor.Get2(i).finalMoveVector.y = 0;
                    _groundSensor.GetEntity(i).Get<VerticalMove>().v = 0;
                }

                
            }
        }
    }
}