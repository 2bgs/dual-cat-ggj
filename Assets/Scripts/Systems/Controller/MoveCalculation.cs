﻿using Components.Controller;
using DualCat.Components.Player.Controller;
using Leopotam.Ecs;
using UnityEngine;

namespace DualCat.Systems.Controller
{
    public class MoveCalculation: IEcsRunSystem
    {
        private EcsFilter<Movable> _movable;
        private EcsFilter<Movable, HorizontalMove> _horizontalMovment;
        private EcsFilter<Movable, VerticalMove> _verticalMovment;
        
        
        public void Run()
        {
            foreach (var i in _horizontalMovment)
            {
                ref var movableComponent = ref _horizontalMovment.Get1(i);
                movableComponent.finalMoveVector.x = _horizontalMovment.Get2(i).v;
            }

            foreach (var i in _verticalMovment)
            {
                ref var movableComponent = ref _verticalMovment.Get1(i);
                movableComponent.finalMoveVector.y += _verticalMovment.Get2(i).v;
            }
        }
    }
}