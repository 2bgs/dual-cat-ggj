﻿using Components.Controller;
using DualCat.Components.Audio;
using DualCat.Components.Enemy.Controller;
using DualCat.Components.Gameplay;
using DualCat.Components.Player.Controller;
using DualCat.So;
using Leopotam.Ecs;
using UnityEngine;
using EnemyCreator = Components.Controller.EnemyCreator;

namespace DualCat.Systems.Enemy
{
    public class ProcessEnemyCreationSystem: IEcsRunSystem
    {
        private EcsFilter<EnemyCreator> _enemyCreator;
        private EcsWorld _world;
        private WorldConfig _worldConfig;
        
        public void Run()
        {
            foreach (var i in _enemyCreator)
            {
                if (!_enemyCreator.Get1(i).Creator.GetIsReload())
                {
                    var charge = _enemyCreator.Get1(i).Creator.CreateCharge();
                    var entity = _world.NewEntity();
                    ref var movableComponent = ref entity.Get<Movable>();
                    ref var enemyComponent = ref entity.Get<EnemyAi>();
                    ref var collidableWithWall = ref entity.Get<CollidableWithWallComponent>();
                    movableComponent.Controller = charge;
                    enemyComponent.AiTools = charge;
                    enemyComponent.A = charge.GetA();
                    enemyComponent.B = charge.GetB();
                    movableComponent.speed = _worldConfig.LazerChargeHorizontalSpeed;
                    collidableWithWall.CollidableWithWall = charge.gameObject.GetComponent<CollidableWithWall>();
                    entity.Get<LazerShoot>();
                }
            }
        }
    }
}