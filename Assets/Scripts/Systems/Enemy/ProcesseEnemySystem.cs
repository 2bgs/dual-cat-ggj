﻿using System;
using Components.Controller;
using Components.Player;
using DualCat.Components.Player;
using DualCat.Components.Player.Controller;
using Leopotam.Ecs;
using UnityEngine;

namespace DualCat.Systems.Input
{
    public class ProcessEnemySystem: IEcsRunSystem
    {
        private EcsFilter<Movable, EnemyAi> _enemy;
        
        public void Run()
        {
            foreach (var i in _enemy)
            {
                if (isNearPoint(_enemy.Get2(i).A, i))
                {
                    _enemy.Get1(i).finalMoveVector.x = _enemy.Get1(i).speed;
                    Rotate(_enemy.GetEntity(i));
                }
                else if (isNearPoint(_enemy.Get2(i).B, i))
                {
                    _enemy.Get1(i).finalMoveVector.x = -_enemy.Get1(i).speed;
                    Rotate(_enemy.GetEntity(i));
                }
            }
        }
        
        private bool isNearPoint(GameObject point, int i)
        {
            if (point == null)
            {
                return false;
            }
            return Math.Abs(point.transform.position.x - _enemy.Get2(i).AiTools.GetPosition().x) < 1;
        }

        private void Rotate(EcsEntity ecsEntity)
        {
            if (ecsEntity.Has<Robot>())
            {
                ref var _robot = ref ecsEntity.Get<Robot>();
                _robot.Rotate = true;
                if (_robot.RotateSign == 0.0f)
                {
                    _robot.RotateSign = -1;
                }
                else
                {
                    _robot.RotateSign = -_robot.RotateSign;
                }
                        
            }
        }
    }
}