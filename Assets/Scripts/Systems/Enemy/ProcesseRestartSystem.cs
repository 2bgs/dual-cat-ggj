﻿using Components.Controller;
using DualCat.Components.GameControl.Controller;
using DualCat.Components.Gameplay;
using Leopotam.Ecs;
using UnityEngine;

namespace DualCat.Systems.Input
{
    public class ProcessRestartSystem: IEcsRunSystem
    {
        private EcsFilter<PlayerCollisionDetector> _playerCollisionDetector;
        private EcsFilter<Restart> _restart;
        private EcsFilter<Death> _fakeDeathFilter;
        
        private Components.Player.Controller.Player _player;
        private UnityComponents.SceneManager _sceneManager;
        private EcsWorld _ecsWorld;

        public void Run()
        {
            if (!_fakeDeathFilter.IsEmpty())
            {
                return;
            }

            foreach (var i in _playerCollisionDetector)
            {
                if (_playerCollisionDetector.Get1(i).Detector.isCollideWithEnemy())
                {
                    _ecsWorld.NewEntity().Get<Restart>();
                }

                if (!_restart.IsEmpty())
                {
                    _player.SwitchDeadMode();
                    _player.gameObject.layer = LayerMask.NameToLayer("Ignore Colliders");
                
                    _sceneManager.RestartScene();
                    
                    _ecsWorld.NewEntity().Get<Death>();
                    _ecsWorld.NewEntity().Get<TakeControl>();
                }
            }
        }
    }
}