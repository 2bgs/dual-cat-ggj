﻿using Components.Controller;
using DualCat.Components.Enemy.Controller;
using DualCat.Components.Player.Controller;
using DualCat.So;
using Leopotam.Ecs;
using UnityEngine;
using EnemyCreator = Components.Controller.EnemyCreator;

namespace DualCat.Systems.Enemy
{
    public class ProcessEnemyCollideWithWallSystem: IEcsRunSystem
    {
        private EcsFilter<CollidableWithWallComponent> _enemyCollidableWithWall;
        private EcsWorld _world;
        private WorldConfig _worldConfig;
        
        public void Run()
        {
            foreach (var i in _enemyCollidableWithWall)
            {
                if (_enemyCollidableWithWall.Get1(i).CollidableWithWall.GetIsCollideWithWall())
                {
                    var gameObject = _enemyCollidableWithWall.Get1(i).CollidableWithWall.GetGameObject();
                    _enemyCollidableWithWall.Get1(i).CollidableWithWall.DestroyObject(gameObject);
                    _enemyCollidableWithWall.GetEntity(i).Destroy();
                }
            }
        }
    }
}