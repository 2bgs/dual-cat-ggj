﻿using System.Collections.Generic;
using Components.Controller;
using DualCat.Components.Audio;
using DualCat.Components.Player.Controller;
using DualCat.So;
using Leopotam.Ecs;

namespace DualCat.Systems.Enemy
{
    public class RobotInitSystem: IEcsInitSystem
    {   
        private List<Components.Enemy.Controller.Robot> _robots;
        private EcsWorld _world;
        private WorldConfig _worldConfig;

        public void Init()
        {
            foreach (var _robot in _robots)
            {
                var entity = _world.NewEntity();
                ref var robot = ref entity.Get<Robot>();
                robot.Speed = _worldConfig.EnemyHorizontalSpeed;
                robot.IRobot = _robot;
                ref var groundSensor = ref entity.Get<CollisionSensor>();
                var enemy = _robot.gameObject.GetComponent<Components.Enemy.Controller.Enemy>();
                groundSensor.Sensor = enemy;
                ref var movableComponent = ref entity.Get<Movable>();
                movableComponent.Controller = enemy;
                movableComponent.speed = _worldConfig.EnemyHorizontalSpeed;
                ref var enemyComponent = ref entity.Get<EnemyAi>();
                enemyComponent.AiTools = enemy;
                entity.Get<RobotMoving>();
            }
        }
    }
}