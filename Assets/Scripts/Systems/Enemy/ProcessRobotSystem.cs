﻿using Components.Controller;
using DualCat.Components.Audio;
using DualCat.Components.Enemy.Controller;
using DualCat.Components.GameControl.Controller;
using DualCat.Components.Player.Controller;
using DualCat.So;
using Leopotam.Ecs;
using UnityEngine;
using EnemyCreator = Components.Controller.EnemyCreator;
using Robot = Components.Controller.Robot;

namespace DualCat.Systems.Enemy
{
    public class ProcessRobotSystem: IEcsRunSystem
    {
        private EcsFilter<Movable, Robot, EnemyAi> _robot;
        private EcsFilter<Death> _death;
        private EcsWorld _world;
        private WorldConfig _worldConfig;

        public void Run()
        {
            foreach (var i in _robot)
            {
                if (_robot.Get2(i).Rotate)
                {
                    _robot.Get2(i).Rotate = _robot.Get2(i).IRobot.RotateModel(_robot.Get2(i).RotateSign);
                }
                var isGoingForward = _robot.Get2(i).IRobot.GetIsTriggeredByPlayer() && !_death.IsEmpty();
                if (isGoingForward)
                {
                    _robot.Get3(i).A = null;
                    _robot.Get3(i).B = null;
                }
                else if (_robot.Get3(i).A == null && _robot.Get3(i).B == null)
                {
                    _robot.Get3(i).A = _robot.Get3(i).AiTools.GetA();
                    _robot.Get3(i).B = _robot.Get3(i).AiTools.GetB();
                }
                
                if (ChangeDirectionToPlayer(i, -_robot.Get1(i).finalMoveVector))
                {
                    _robot.Get1(i).finalMoveVector.x = -_robot.Get1(i).finalMoveVector.x;
                    RotateRobot(i);
                }
            
                var wallRayCast = rayCast(i, _robot.Get1(i).finalMoveVector, 1f);
                if (wallRayCast.collider)
                {
                    Debug.Log(wallRayCast.collider.tag);
                }
                if (wallRayCast.collider && wallRayCast.collider.CompareTag("wall"))
                {
                    _robot.Get1(i).finalMoveVector.x = -_robot.Get1(i).finalMoveVector.x;
                    RotateRobot(i);
                }
            }
        }

        private void RotateRobot(int i)
        {
            _robot.Get2(i).Rotate = true;
            if (_robot.Get2(i).RotateSign == 0.0f)
            {
                _robot.Get2(i).RotateSign = -1;
            }
            else
            {
                _robot.Get2(i).RotateSign = -_robot.Get2(i).RotateSign;
            }
        }
        private bool ChangeDirectionToPlayer(int i, Vector3 robotPlayerDirection)
        {
            float rayDistance = 100f;
           
            RaycastHit hit = rayCast(i, robotPlayerDirection, rayDistance);
            return hit.collider &&
                   hit.collider.CompareTag("Player") &&
                   hit.distance > 7.0f &&
                   robotPlayerDirection.x * _robot.Get1(i).finalMoveVector.x < 0 &&
                   checkIfPlayerIsInPatrolArea(hit, i) ;
        }

        private bool checkIfPlayerIsInPatrolArea(RaycastHit hit, int i)
        {
            Vector3 midlePoint = 0.5f * (_robot.Get3(i).AiTools.GetB().transform.position + _robot.Get3(i).AiTools.GetA().transform.position);
            float radius = Vector3.Magnitude(_robot.Get3(i).AiTools.GetA().transform.position - midlePoint);
            float distanceToPlayer = Vector3.Magnitude(midlePoint - hit.point);
            return distanceToPlayer < radius;
        }
        
        private RaycastHit rayCast(int i, Vector3 checkDirection, float rayDistance)
        {
            Vector3 position = _robot.Get3(i).AiTools.GetPosition() + 1.5f * Vector3.up;
            RaycastHit hit;
            
            Debug.DrawRay(position, checkDirection, Color.red, 1.0f);
            if (Physics.Raycast(position, checkDirection, out hit, rayDistance))
            {
                return hit;
            }

            return hit;
        }
    }
}