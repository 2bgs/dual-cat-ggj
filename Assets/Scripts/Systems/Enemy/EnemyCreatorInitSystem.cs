﻿using System.Collections.Generic;
using Components.Controller;
using Leopotam.Ecs;

namespace DualCat.Systems.Enemy
{
    public class EnemyCreatorInitSystem: IEcsInitSystem
    {
        private List<Components.Enemy.Controller.EnemyCreator> _enemyCreators;
        private EcsWorld _world;

        public void Init()
        {
            foreach (var _enemyCreator in _enemyCreators)
            {
                var entity = _world.NewEntity();
                ref var enemyCreator = ref entity.Get<EnemyCreator>();
                enemyCreator.Creator = _enemyCreator;
            }
        }
    }
}