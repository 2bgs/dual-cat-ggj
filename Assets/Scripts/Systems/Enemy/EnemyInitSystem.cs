﻿using System.Collections.Generic;
using Components.Controller;
using DualCat.Components.Player.Controller;
using DualCat.So;
using Leopotam.Ecs;

namespace DualCat.Systems.Enemy
{
    public class EnemyInitSystem: IEcsInitSystem
    {   
        private List<Components.Enemy.Controller.Enemy> _enemies;
        private EcsWorld _world;
        private WorldConfig _worldConfig;

        public void Init()
        {
            foreach (var _enemy in _enemies)
            {
                var entity = _world.NewEntity();
                ref var movableComponent = ref entity.Get<Movable>();
                ref var enemyComponent = ref entity.Get<EnemyAi>();
                movableComponent.Controller = _enemy;
                movableComponent.speed = _worldConfig.EnemyHorizontalSpeed;
                enemyComponent.AiTools = _enemy;
                enemyComponent.A = _enemy.GetA();
                enemyComponent.B = _enemy.GetB();
            }
        }
    }
}