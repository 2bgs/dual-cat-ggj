﻿using System;
using DualCat.Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems.Goal
{
    public class GoalAnimationSystem: IEcsRunSystem
    {
        private EcsFilter<DualCat.Components.Goal.Goal, UnityTransform> _goal;
        public void Run()
        {
            foreach (var i in _goal)
            {
                ref var _goalMono = ref _goal.Get1(i);
                ref var _transform = ref _goal.Get2(i);
                _transform.Transform.Rotate(Vector3.up, _goalMono.GoalMono.RotationSpeed * Time.deltaTime);
                _transform.Transform.Translate(new Vector3(0, MovingFunction(_goalMono, _transform), 0));
            }
        }

        private static float MovingFunction(DualCat.Components.Goal.Goal _goalMono, UnityTransform unityTransform)
        {
            return (float)(Math.Sin(_goalMono.GoalMono.WoblingSpeed * (Time.time)) * (_goalMono.GoalMono.Amplituted * _goalMono.GoalMono.WoblingSpeed) * Time.deltaTime);
        }
    }
}