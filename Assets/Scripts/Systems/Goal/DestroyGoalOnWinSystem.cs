﻿using DualCat.Components;
using DualCat.Components.Gameplay;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems.Goal
{
    public class DestroyGoalOnWinSystem: IEcsRunSystem
    {
        private EcsFilter<Win> _winFilter;
        private EcsFilter<DualCat.Components.Goal.Goal, UnityGameObject> _goals;
        public void Run()
        {
            if (_winFilter.IsEmpty()) return;

            foreach (var i in _goals)
            {
                Object.Destroy(_goals.Get2(i).GameObject);
                _goals.GetEntity(i).Destroy();
            }
        }
    }
}