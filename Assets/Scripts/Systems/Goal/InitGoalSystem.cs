﻿using DualCat.Components;
using Leopotam.Ecs;
using UnityComponents;
using UnityComponents.Goal;

namespace Systems.Goal
{
    public class InitGoalSystem: IEcsInitSystem
    {
        private SceneManager _sceneManager;
        private EcsWorld _ecsWorld;
        
        public void Init()
        {
            
            foreach (var g in _sceneManager.GetComponentsOnSceneByType<GoalMono>())
            {
                var entity = _ecsWorld.NewEntity();
                entity.Get<DualCat.Components.Goal.Goal>().GoalMono = g;
                entity.Get<UnityGameObject>().GameObject = g.gameObject;
                entity.Get<UnityTransform>().Transform = g.gameObject.transform;
            }
        }
    }
}