﻿mergeInto(LibraryManager.library, {    
    writeData: function(name, data) {
        window.localStorage.setItem(Pointer_stringify(name), Pointer_stringify(data));
    },  
    
    logData: function(data) {
        console.log(Pointer_stringify(data));
    },
    
    readData: function(name) {
        var returnStr = window.localStorage.getItem(Pointer_stringify(name));
        if (!returnStr) {
            return "";
        }
        var bufferSize = lengthBytesUTF8(returnStr) + 1;
        var buffer = _malloc(bufferSize);
        stringToUTF8(returnStr, buffer, bufferSize);
        return buffer;
    },
    
    setTitle: function(title) {
        document.title = Pointer_stringify(title);
    }
})